//
//  BusTimeEntry+CoreDataProperties.swift
//  BusTime
//
//  Created by Chris Wong on 27/9/15.
//  Copyright © 2015 Chris Wong. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension BusTimeEntry {

    @NSManaged var entryDateTime: NSDate?
    @NSManaged var isUpperDeck: NSNumber?
    @NSManaged var isLowerDeck: NSNumber?
    @NSManaged var remark: String?

}
