//
//  MoreViewController.swift
//  BusTime
//
//  Created by Chris Wong on 20/12/2015.
//  Copyright © 2015 Chris Wong. All rights reserved.
//

import UIKit
import MessageUI

class MoreViewController: UIViewController, MFMailComposeViewControllerDelegate  {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func backUpButtonDidTap(sender: UIButton) {
        
        if (MFMailComposeViewController.canSendMail()) {
            let mailComposeViewController = MFMailComposeViewController()
            mailComposeViewController.mailComposeDelegate = self
            mailComposeViewController.setSubject("Hello")
            mailComposeViewController.setMessageBody("hihi", isHTML: true)
            mailComposeViewController.setToRecipients(["chriswong.broadband@gmail.com"])
            let data = CSVExporter.export().dataUsingEncoding(NSUTF8StringEncoding)
            mailComposeViewController.addAttachmentData(data!, mimeType: "test/csv", fileName: "export.csv")
            presentViewController(mailComposeViewController, animated: true) {}
        }
    }
    
    func mailComposeController(controller: MFMailComposeViewController, didFinishWithResult result: MFMailComposeResult, error: NSError?) {
        
        switch result.rawValue {
        case MFMailComposeResultSent.rawValue:
            print("Mail sent")
        default:
            break
        }
        
        dismissViewControllerAnimated(true) {}
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
