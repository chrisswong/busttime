//
//  BusTimeDataSource.swift
//  BusTime
//
//  Created by Chris Wong on 20/12/2015.
//  Copyright © 2015 Chris Wong. All rights reserved.
//

import UIKit
import CoreData

class BusTimeDataSource :NSObject {
    
    static func retrieveData() -> [BusTimeEntry]? {
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        
        let managedContext = appDelegate.managedObjectContext
        
        let fetchRequest = NSFetchRequest(entityName: "BusTimeEntry")
        
        let entryDateTimeSortDescriptior = NSSortDescriptor(key: "entryDateTime", ascending: false)
        
        fetchRequest.sortDescriptors = [entryDateTimeSortDescriptior]
        
        do {
           let fetchResults = try managedContext.executeFetchRequest(fetchRequest) as? [BusTimeEntry]
            return fetchResults
        }
        
        catch {
            print("Fetch error")
            return nil
        }
    }
    
    static func addRecord(time: NSDate?,  upper:Bool, lower:Bool, remark:String? ) -> Bool {
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        
        let managedContext = appDelegate.managedObjectContext
        
        let entity = NSEntityDescription.entityForName("BusTimeEntry", inManagedObjectContext: managedContext)
        
        let entry = NSManagedObject(entity: entity!, insertIntoManagedObjectContext: managedContext)
        
        entry.setValue(time, forKey: "entryDateTime")
        
        entry.setValue( lower, forKey: "isLowerDeck")
        entry.setValue( upper, forKey: "isUpperDeck")
        entry.setValue( remark, forKey: "remark")
        
        do {
            try managedContext.save()
            return true
        }
        catch {
            print("save fail")
            return false
        }
    }

}
