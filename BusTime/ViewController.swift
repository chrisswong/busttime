//
//  ViewController.swift
//  BusTime
//
//  Created by Chris Wong on 27/9/15.
//  Copyright © 2015 Chris Wong. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    let cellIdentifier = "timeCell"

    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var truncateBarButton: UIBarButtonItem!
    var entries = [NSManagedObject]()
    var dateFormatter = NSDateFormatter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        dateFormatter.dateFormat = "YYYY-MM-dd HH:mm"
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        retrieveData()
      
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: Private
    
    func retrieveData() {
        if let results = BusTimeDataSource.retrieveData()
        {
            entries = results
            if entries.count == 0 {
                truncateBarButton.enabled = false
            } else {
                truncateBarButton.enabled = true
                }
            }
        else
        {
            print("empty results")
        }
        tableView.reloadData()
    }
    
    // MARK: UITableViewDataSource
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return entries.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath)
        let entry = entries[indexPath.row] as! BusTimeEntry
        cell.textLabel?.text = dateFormatter.stringFromDate(entry.entryDateTime!)
        return cell
    }
    
    // MARK: UITableViewDelegate
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let entry = entries[indexPath.row] as! BusTimeEntry
        var message = ""
        if let lowerDeckFlag = entry.isLowerDeck?.boolValue {
            if lowerDeckFlag.boolValue {
                message += "lower : ✔️\n"
            }
        }
        
        if let upperDeckFlag = entry.isUpperDeck?.boolValue {
            if upperDeckFlag.boolValue {
                message += "upper : ✔️\n"
            }
        }
        
        if let remark = entry.remark {
            if remark != "" {
                message += "remark : " + remark
            }

        }
        
        
        let alert = UIAlertController(title: "", message: message, preferredStyle: .Alert)
        let alertActionCancel = UIAlertAction(title: "Cancel", style: .Cancel) { (action) -> Void in
            
        }
        alert.addAction(alertActionCancel)
        presentViewController(alert, animated: true) {}
    }
    
    @IBAction func truncateButtonDidTap(sender: UIBarButtonItem) {
        let alert = UIAlertController(title: "Are you sure to clear all entries?", message: "", preferredStyle: .Alert)
        let alertActionYes = UIAlertAction(title: "Yes", style: .Destructive) { [unowned self] action in
            
            let fetchRequest = NSFetchRequest(entityName: "BusTimeEntry")
            let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
            let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
            let managedContext = appDelegate.managedObjectContext
            do {
                try appDelegate.persistentStoreCoordinator.executeRequest(deleteRequest, withContext: managedContext)
            } catch let error as NSError {
                // TODO: handle the error
                print("Can not truncate table : \(error.localizedDescription)")
            }
            self.retrieveData()
        }
        
        alert.addAction(alertActionYes)
        
        let alertActionCancel = UIAlertAction(title: "Cancel", style: .Cancel) { (action) -> Void in
            print("alert is cancel")
        }
        
        alert.addAction(alertActionCancel)
        
        presentViewController(alert, animated: true) {
            
        }
    }
}

