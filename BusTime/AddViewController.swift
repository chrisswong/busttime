//
//  AddViewController.swift
//  BusTime
//
//  Created by Chris Wong on 27/9/15.
//  Copyright © 2015 Chris Wong. All rights reserved.
//

import UIKit
import CoreData

class AddViewController: UIViewController {

    @IBOutlet weak var dateTimeLabel: UILabel!
    @IBOutlet weak var upperDeckSwitch: UISwitch!
    @IBOutlet weak var lowerDeckSwitch: UISwitch!
    @IBOutlet weak var remarkTextView: UITextView!
    
    @IBOutlet weak var datePicker: UIDatePicker!
    
    var dateFormatter:NSDateFormatter = NSDateFormatter()
    
    var isDatePickerValueChanged:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        let currentDateTime = NSDate()
//        let dateFormatter = NSDateFormatter()
        dateFormatter.timeZone = NSTimeZone.localTimeZone()
        dateFormatter.dateFormat = "YYYY-MM-dd HH:mm"
        self.dateTimeLabel.text = "Current date time : \n" + dateFormatter.stringFromDate(currentDateTime)
        
        datePicker.maximumDate = NSDate()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func datePickerValueChanged(sender: UIDatePicker) {
        isDatePickerValueChanged = true
    }
    
    @IBAction func saveButtonDidTap(sender: UIBarButtonItem) {

        if isDatePickerValueChanged {
            let alert = UIAlertController(title: "", message: "Are you sure using \n \(dateFormatter.stringFromDate(datePicker.date)) ", preferredStyle: .Alert)
            let confirmAlertAction = UIAlertAction(title: "Yes", style: .Default, handler: { action in
                self.saveRecord()
            })
            let cancelAlertAction = UIAlertAction(title: "Cancel", style: .Cancel) { action in }
            
            alert.addAction(confirmAlertAction)
            alert.addAction(cancelAlertAction)
            
            presentViewController(alert, animated: true) {}
        } else {
            saveRecord()
        }
    }
    
    @IBAction func cancelButtonDidTap(sender: UIBarButtonItem) {
        
        dismissViewControllerAnimated(true) {}
    }
    
    func saveRecord() {
        
        if BusTimeDataSource.addRecord(!isDatePickerValueChanged ? NSDate() : datePicker.date,
                                    upper: upperDeckSwitch.on,
                                    lower: lowerDeckSwitch.on,
                                    remark: remarkTextView.text) {
            dismissViewControllerAnimated(true) {}
        } else {
            let alert = UIAlertController(title: "", message: "save failed", preferredStyle: .Alert)
            _ = UIAlertAction(title: "OK", style: .Cancel) { action in }
            presentViewController(alert, animated: true) {}
            
        }
        

    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
