//
//  CSVExporter.swift
//  BusTime
//
//  Created by Chris Wong on 20/12/2015.
//  Copyright © 2015 Chris Wong. All rights reserved.
//

import UIKit

class CSVExporter: NSObject {
    
    static func export() -> String {
        
        let entries = BusTimeDataSource.retrieveData()
        var out = ""
        print(entries?.count)
        var count = 0
        if let result = entries {
            let titles:[String] = ["Date Time", "is upper","is lower","remark"]
            for i in 0..<4 {
                out += "\"" + titles[i] + "\""
                
                if i != 3 {
                    out += ","
                }
                
            }
            
            out += "\n"
            
            for entry in result
            {
                out += "\"\(entry.entryDateTime!)\","
                if ((entry.isUpperDeck?.boolValue) != nil) {
                    out += "\"Y"
                } else {
                    out += "\"N"
                }
                out += "\",\""
                
                if ((entry.isLowerDeck?.boolValue) != nil)  {
                    out += "Y"
                } else {
                    out += "N"
                }
                out += "\",\""
                
                if let r = entry.remark {
                    out += r + "\""
                } else {
                    out += "\"\""
                }
                
                out += "\n"
//                ((out += "\(entry.isUpperDeck ? "Y" : "N"),") != nil)
//                out += (entry.isLowerDeck ? "Y" : "N") + ","
//                out += entry.remark!
                
                count++
                print(out)
            }
        }
        
        print("count = \(count)")
        return out
    }
}
